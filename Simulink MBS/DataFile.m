% Simscape(TM) Multibody(TM) version: 7.1

% This is a model data file derived from a Simscape Multibody Import XML file using the smimport function.
% The data in this file sets the block parameter values in an imported Simscape Multibody model.
% For more information on this file, see the smimport function help page in the Simscape Multibody documentation.
% You can modify numerical values, but avoid any other changes to this file.
% Do not add code to this file. Do not edit the physical units shown in comments.

%%%VariableName:smiData


%============= RigidTransform =============%

%Initialize the RigidTransform structure array by filling in null values.
smiData.RigidTransform(51).translation = [0.0 0.0 0.0];
smiData.RigidTransform(51).angle = 0.0;
smiData.RigidTransform(51).axis = [0.0 0.0 0.0];
smiData.RigidTransform(51).ID = '';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(1).translation = [0 55.999999999999993 0];  % mm
smiData.RigidTransform(1).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(1).axis = [0.57735026918962584 -0.57735026918962584 0.57735026918962584];
smiData.RigidTransform(1).ID = 'B[Base-1:-:Waist-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(2).translation = [-2.652183510574496e-15 -1 1.3674350288741039e-14];  % mm
smiData.RigidTransform(2).angle = 2.0943951023931957;  % rad
smiData.RigidTransform(2).axis = [0.57735026918962584 -0.57735026918962573 0.57735026918962573];
smiData.RigidTransform(2).ID = 'F[Base-1:-:Waist-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(3).translation = [-5.0999999999999659 29.900000000000002 0];  % mm
smiData.RigidTransform(3).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(3).axis = [0.57735026918962584 -0.57735026918962584 0.57735026918962584];
smiData.RigidTransform(3).ID = 'B[Servo Motor Micro  9g-1:-:Arm 03-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(4).translation = [-4.9999999999999787 5.3290705182007514e-15 1.8207657603852567e-14];  % mm
smiData.RigidTransform(4).angle = 2.0943951023931957;  % rad
smiData.RigidTransform(4).axis = [0.57735026918962584 -0.57735026918962584 0.57735026918962562];
smiData.RigidTransform(4).ID = 'F[Servo Motor Micro  9g-1:-:Arm 03-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(5).translation = [0 27.999999998507757 -14.000000000000002];  % mm
smiData.RigidTransform(5).angle = 0;  % rad
smiData.RigidTransform(5).axis = [0 0 0];
smiData.RigidTransform(5).ID = 'B[Arm 03-1:-:Servo Motor Micro  9g-2]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(6).translation = [-5.1000000000000192 28.550000000000011 -1.1723955140041653e-13];  % mm
smiData.RigidTransform(6).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(6).axis = [0.57735026918962562 -0.5773502691896254 0.57735026918962629];
smiData.RigidTransform(6).ID = 'F[Arm 03-1:-:Servo Motor Micro  9g-2]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(7).translation = [-5.1000000000000005 29.900000000000006 0];  % mm
smiData.RigidTransform(7).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(7).axis = [0.57735026918962584 -0.57735026918962584 0.57735026918962584];
smiData.RigidTransform(7).ID = 'B[Servo Motor Micro  9g-2:-:Gripper base-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(8).translation = [13.999999999999993 -9.0000000000000231 -63.000000000000043];  % mm
smiData.RigidTransform(8).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(8).axis = [0.57735026918962584 0.57735026918962584 0.57735026918962584];
smiData.RigidTransform(8).ID = 'F[Servo Motor Micro  9g-2:-:Gripper base-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(9).translation = [31.90000000000002 0 -24.999999999999993];  % mm
smiData.RigidTransform(9).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(9).axis = [0.57735026918962584 -0.57735026918962584 0.57735026918962584];
smiData.RigidTransform(9).ID = 'B[Gripper base-1:-:Servo Motor Micro  9g-3]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(10).translation = [-5.100000000000013 23.549999999999972 3.3306690738754696e-15];  % mm
smiData.RigidTransform(10).angle = 2.0943951023931957;  % rad
smiData.RigidTransform(10).axis = [0.57735026918962595 0.57735026918962595 -0.57735026918962551];
smiData.RigidTransform(10).ID = 'F[Gripper base-1:-:Servo Motor Micro  9g-3]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(11).translation = [5.0000000000000009 5.0000000000000044 -24.999999999999982];  % mm
smiData.RigidTransform(11).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(11).axis = [0.57735026918962584 -0.57735026918962584 0.57735026918962584];
smiData.RigidTransform(11).ID = 'B[Gripper base-1:-:gear1-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(12).translation = [-10.1063425538311 -3.5000000000021783 -4.6185277824406512e-14];  % mm
smiData.RigidTransform(12).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(12).axis = [0.57735026918962584 -0.57735026918962562 0.57735026918962595];
smiData.RigidTransform(12).ID = 'F[Gripper base-1:-:gear1-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(13).translation = [31.90000000000002 0 -24.999999999999993];  % mm
smiData.RigidTransform(13).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(13).axis = [0.57735026918962584 -0.57735026918962584 0.57735026918962584];
smiData.RigidTransform(13).ID = 'B[Gripper base-1:-:gear2-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(14).translation = [-10.106342553831119 -8.4999999999998916 -1.7652546091539989e-14];  % mm
smiData.RigidTransform(14).angle = 2.094395102393197;  % rad
smiData.RigidTransform(14).axis = [0.57735026918962629 -0.57735026918962529 0.57735026918962573];
smiData.RigidTransform(14).ID = 'F[Gripper base-1:-:gear2-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(15).translation = [13.000000000000011 5.0000000000000604 -5.0000000000000044];  % mm
smiData.RigidTransform(15).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(15).axis = [0.57735026918962584 -0.57735026918962584 0.57735026918962584];
smiData.RigidTransform(15).ID = 'B[Gripper base-1:-:grip link 1-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(16).translation = [31.000000000000018 8.9999999999999467 -6.2172489379008766e-15];  % mm
smiData.RigidTransform(16).angle = 2.0943951023931948;  % rad
smiData.RigidTransform(16).axis = [0.57735026918962551 -0.57735026918962629 0.57735026918962562];
smiData.RigidTransform(16).ID = 'F[Gripper base-1:-:grip link 1-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(17).translation = [23.000000000000014 5.0000000000000604 -5.0000000000000187];  % mm
smiData.RigidTransform(17).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(17).axis = [0.57735026918962584 -0.57735026918962584 0.57735026918962584];
smiData.RigidTransform(17).ID = 'B[Gripper base-1:-:grip link 1-2]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(18).translation = [30.999999999999989 9.0000000000001048 7.1054273576010019e-15];  % mm
smiData.RigidTransform(18).angle = 2.0943951023931962;  % rad
smiData.RigidTransform(18).axis = [0.57735026918962606 -0.57735026918962484 0.5773502691896264];
smiData.RigidTransform(18).ID = 'F[Gripper base-1:-:grip link 1-2]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(19).translation = [23.000000000000014 5.0000000000000604 -5.0000000000000187];  % mm
smiData.RigidTransform(19).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(19).axis = [0.57735026918962584 -0.57735026918962584 0.57735026918962584];
smiData.RigidTransform(19).ID = 'B[Gripper base-1:-:grip link 1-3]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(20).translation = [31 -3.5000000000000204 8.8817841970012523e-15];  % mm
smiData.RigidTransform(20).angle = 2.0943951023931957;  % rad
smiData.RigidTransform(20).axis = [0.57735026918962584 -0.57735026918962529 0.57735026918962606];
smiData.RigidTransform(20).ID = 'F[Gripper base-1:-:grip link 1-3]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(21).translation = [13.000000000000011 5.0000000000000604 -5.0000000000000044];  % mm
smiData.RigidTransform(21).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(21).axis = [0.57735026918962584 -0.57735026918962584 0.57735026918962584];
smiData.RigidTransform(21).ID = 'B[Gripper base-1:-:grip link 1-4]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(22).translation = [-1.557087792036782e-14 -3.5000000000000551 5.3429483060085659e-15];  % mm
smiData.RigidTransform(22).angle = 2.0943951023931966;  % rad
smiData.RigidTransform(22).axis = [0.57735026918962618 -0.57735026918962518 0.57735026918962595];
smiData.RigidTransform(22).ID = 'F[Gripper base-1:-:grip link 1-4]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(23).translation = [-5.0999999999999659 29.900000000000002 0];  % mm
smiData.RigidTransform(23).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(23).axis = [0.57735026918962584 -0.57735026918962584 0.57735026918962584];
smiData.RigidTransform(23).ID = 'B[Servo Motor Micro  9g-1:-:Arm 02 v3-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(24).translation = [-5.3000000000000256 56.850000000000023 5.5000000000000284];  % mm
smiData.RigidTransform(24).angle = 2.0943951023931957;  % rad
smiData.RigidTransform(24).axis = [0.57735026918962584 -0.57735026918962584 0.57735026918962573];
smiData.RigidTransform(24).ID = 'F[Servo Motor Micro  9g-1:-:Arm 02 v3-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(25).translation = [20.643657446168877 4.0000000000000036 0];  % mm
smiData.RigidTransform(25).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(25).axis = [-0.57735026918962584 -0.57735026918962584 -0.57735026918962584];
smiData.RigidTransform(25).ID = 'B[gear1-1:-:gear2-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(26).translation = [47.206475788080198 4.0000000000023936 -6.2119895386330715];  % mm
smiData.RigidTransform(26).angle = 2.0369932721527699;  % rad
smiData.RigidTransform(26).axis = [-0.61627382633353178 -0.55686918166488097 -0.55686918166488064];
smiData.RigidTransform(26).ID = 'F[gear1-1:-:gear2-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(27).translation = [0 4.0000000000000586 0];  % mm
smiData.RigidTransform(27).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(27).axis = [0.57735026918962584 -0.57735026918962584 0.57735026918962584];
smiData.RigidTransform(27).ID = 'B[grip link 1-1:-:Gripper 1-2]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(28).translation = [8.500000000002057 4.9999999999999929 -27.000000000000007];  % mm
smiData.RigidTransform(28).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(28).axis = [0.57735026918962618 0.57735026918962562 0.5773502691896254];
smiData.RigidTransform(28).ID = 'F[grip link 1-1:-:Gripper 1-2]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(29).translation = [0 4.0000000000000586 0];  % mm
smiData.RigidTransform(29).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(29).axis = [0.57735026918962584 -0.57735026918962584 0.57735026918962584];
smiData.RigidTransform(29).ID = 'B[grip link 1-2:-:Gripper 1-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(30).translation = [-1.021405182655144e-14 4.999999999999984 -26.999999999999968];  % mm
smiData.RigidTransform(30).angle = 2.0943951023931944;  % rad
smiData.RigidTransform(30).axis = [-0.57735026918962606 -0.57735026918962529 0.57735026918962584];
smiData.RigidTransform(30).ID = 'F[grip link 1-2:-:Gripper 1-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(31).translation = [0 4.0000000000001146 0];  % mm
smiData.RigidTransform(31).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(31).axis = [-0.57735026918962584 -0.57735026918962584 -0.57735026918962584];
smiData.RigidTransform(31).ID = 'B[grip link 1-3:-:grip link 1-4]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(32).translation = [-40.999750833599201 4.0000000000001101 0.046998057154530315];  % mm
smiData.RigidTransform(32).angle = 2.0943951023931948;  % rad
smiData.RigidTransform(32).axis = [-0.57735026918962562 -0.57735026918962618 -0.57735026918962562];
smiData.RigidTransform(32).ID = 'F[grip link 1-3:-:grip link 1-4]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(33).translation = [8.499999999999952 5.0000000000000044 -26.999999999999982];  % mm
smiData.RigidTransform(33).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(33).axis = [-0.57735026918962584 -0.57735026918962584 0.57735026918962584];
smiData.RigidTransform(33).ID = 'B[Gripper 1-1:-:grip link 1-3]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(34).translation = [-8.6132795134024347e-15 -5.7616803825586962e-14 -1.55301765671306e-14];  % mm
smiData.RigidTransform(34).angle = 2.0943951023931957;  % rad
smiData.RigidTransform(34).axis = [0.57735026918962573 -0.57735026918962573 0.57735026918962562];
smiData.RigidTransform(34).ID = 'F[Gripper 1-1:-:grip link 1-3]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(35).translation = [8.499999999999952 4.9999999999999902 -26.999999999999996];  % mm
smiData.RigidTransform(35).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(35).axis = [-0.57735026918962584 -0.57735026918962584 0.57735026918962584];
smiData.RigidTransform(35).ID = 'B[Gripper 1-2:-:grip link 1-4]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(36).translation = [31.000000000000011 -8.4999999999978044 -3.5527136788005009e-15];  % mm
smiData.RigidTransform(36).angle = 2.0943951023931948;  % rad
smiData.RigidTransform(36).axis = [-0.57735026918962551 -0.57735026918962629 -0.57735026918962551];
smiData.RigidTransform(36).ID = 'F[Gripper 1-2:-:grip link 1-4]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(37).translation = [-40.593272041075785 0 4.0136804107666206];  % mm
smiData.RigidTransform(37).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(37).axis = [-0.57735026918962584 -0.57735026918962584 -0.57735026918962584];
smiData.RigidTransform(37).ID = 'B[gear2-1:-:Gripper 1-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(38).translation = [8.4999999999997868 5.0000000000000151 -5.0000000000000115];  % mm
smiData.RigidTransform(38).angle = 2.0943951023931962;  % rad
smiData.RigidTransform(38).axis = [0.57735026918962618 0.57735026918962606 0.57735026918962518];
smiData.RigidTransform(38).ID = 'F[gear2-1:-:Gripper 1-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(39).translation = [20.643657446168877 4.0000000000000036 0];  % mm
smiData.RigidTransform(39).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(39).axis = [0.57735026918962584 -0.57735026918962584 0.57735026918962584];
smiData.RigidTransform(39).ID = 'B[gear1-1:-:Gripper 1-2]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(40).translation = [-3.9999999999999822 4.9999999999999734 -4.9999999999999911];  % mm
smiData.RigidTransform(40).angle = 2.0943951023931957;  % rad
smiData.RigidTransform(40).axis = [0.57735026918962573 0.57735026918962595 0.57735026918962562];
smiData.RigidTransform(40).ID = 'F[gear1-1:-:Gripper 1-2]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(41).translation = [0 37.999999999999993 12.999999999999998];  % mm
smiData.RigidTransform(41).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(41).axis = [-0.57735026918962584 -0.57735026918962584 -0.57735026918962584];
smiData.RigidTransform(41).ID = 'B[Base-1:-:Servo Motor MG996R-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(42).translation = [-10.250000000000014 28.800000000000004 -13];  % mm
smiData.RigidTransform(42).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(42).axis = [-0.57735026918962584 0.57735026918962584 0.57735026918962584];
smiData.RigidTransform(42).ID = 'F[Base-1:-:Servo Motor MG996R-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(43).translation = [-18.000000000000004 0 0];  % mm
smiData.RigidTransform(43).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(43).axis = [-0.57735026918962584 -0.57735026918962584 0.57735026918962584];
smiData.RigidTransform(43).ID = 'B[Waist-1:-:Servo Motor MG996R-2]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(44).translation = [28.071642444851708 31.199999999999989 -18.887902064603974];  % mm
smiData.RigidTransform(44).angle = 2.5935642459694805;  % rad
smiData.RigidTransform(44).axis = [0.28108463771482034 -0.678598344545847 0.67859834454584689];
smiData.RigidTransform(44).ID = 'F[Waist-1:-:Servo Motor MG996R-2]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(45).translation = [-10.249999999999995 47.400000000000027 0];  % mm
smiData.RigidTransform(45).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(45).axis = [0.57735026918962584 -0.57735026918962584 0.57735026918962584];
smiData.RigidTransform(45).ID = 'B[Servo Motor MG996R-2:-:Arm 01-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(46).translation = [9.5825041189050964e-15 -2.8353388305882589e-14 2.3840864633590888e-14];  % mm
smiData.RigidTransform(46).angle = 3.1415926535897931;  % rad
smiData.RigidTransform(46).axis = [-1 -1.0815908147357745e-33 5.1361829758201253e-17];
smiData.RigidTransform(46).ID = 'F[Servo Motor MG996R-2:-:Arm 01-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(47).translation = [0 120.00000000000003 -0.0010000000000010001];  % mm
smiData.RigidTransform(47).angle = 0;  % rad
smiData.RigidTransform(47).axis = [0 0 0];
smiData.RigidTransform(47).ID = 'B[Arm 01-1:-:Servo Motor MG996R-7]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(48).translation = [-10.250000000000014 47.398999999999972 -3.5527136788005009e-14];  % mm
smiData.RigidTransform(48).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(48).axis = [-0.57735026918962584 -0.57735026918962573 -0.57735026918962573];
smiData.RigidTransform(48).ID = 'F[Arm 01-1:-:Servo Motor MG996R-7]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(49).translation = [27.299999999999997 31.200000000000003 -9.2500000000000924];  % mm
smiData.RigidTransform(49).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(49).axis = [-0.57735026918962584 -0.57735026918962584 -0.57735026918962584];
smiData.RigidTransform(49).ID = 'B[Servo Motor MG996R-7:-:Arm 02 v3-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(50).translation = [8.9999999999999964 2.7999999999999901 11.000000000000007];  % mm
smiData.RigidTransform(50).angle = 3.1415926535897931;  % rad
smiData.RigidTransform(50).axis = [0 1 -1.3877787807814457e-16];
smiData.RigidTransform(50).ID = 'F[Servo Motor MG996R-7:-:Arm 02 v3-1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(51).translation = [40.779253635064286 36.107574107867663 92.879565691767866];  % mm
smiData.RigidTransform(51).angle = 0;  % rad
smiData.RigidTransform(51).axis = [0 0 0];
smiData.RigidTransform(51).ID = 'RootGround[Base-1]';


%============= Solid =============%
%Center of Mass (CoM) %Moments of Inertia (MoI) %Product of Inertia (PoI)

%Initialize the Solid structure array by filling in null values.
smiData.Solid(12).mass = 0.0;
smiData.Solid(12).CoM = [0.0 0.0 0.0];
smiData.Solid(12).MoI = [0.0 0.0 0.0];
smiData.Solid(12).PoI = [0.0 0.0 0.0];
smiData.Solid(12).color = [0.0 0.0 0.0];
smiData.Solid(12).opacity = 0.0;
smiData.Solid(12).ID = '';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(1).mass = 0.0071285815431194298;  % kg
smiData.Solid(1).CoM = [-0.42843487706856681 12.510643062129503 0];  % mm
smiData.Solid(1).MoI = [0.45814644939764348 0.42765081401936345 0.70781861394704548];  % kg*mm^2
smiData.Solid(1).PoI = [0 0 0.035781849855651124];  % kg*mm^2
smiData.Solid(1).color = [0.0 0.2 1.0];
smiData.Solid(1).opacity = 0.5;
smiData.Solid(1).ID = 'Servo Motor Micro  9g*:*Default';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(2).mass = 0.00079876241318469084;  % kg
smiData.Solid(2).CoM = [15.500000000000007 2 0];  % mm
smiData.Solid(2).MoI = [0.0036325042236300983 0.10442422565960367 0.10292175453779942];  % kg*mm^2
smiData.Solid(2).PoI = [0 0 0];  % kg*mm^2
smiData.Solid(2).color = [0.2 0.2 0.2];
smiData.Solid(2).opacity = 1;
smiData.Solid(2).ID = 'grip link 1*:*Default';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(3).mass = 0.064392689835693517;  % kg
smiData.Solid(3).CoM = [-0.13010640517118308 58.590055934236545 7.7687950238576207];  % mm
smiData.Solid(3).MoI = [110.15257046900959 9.2537956247527973 115.09551025859406];  % kg*mm^2
smiData.Solid(3).PoI = [0.13194957348981756 -0.022606570487793718 -0.6617465280356204];  % kg*mm^2
smiData.Solid(3).color = [0.0 0.2 1.0];
smiData.Solid(3).opacity = 1;
smiData.Solid(3).ID = 'Arm 01*:*Default';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(4).mass = 0.049948717856823657;  % kg
smiData.Solid(4).CoM = [0.028024924378138256 4.6441447657802728 5.4024359593168887];  % mm
smiData.Solid(4).MoI = [54.11466352880349 9.2674326962652582 57.648018133508842];  % kg*mm^2
smiData.Solid(4).PoI = [-0.29652409821283932 -6.3527957862201235e-05 -0.06943130476054514];  % kg*mm^2
smiData.Solid(4).color = [0.0 0.2 1.0];
smiData.Solid(4).opacity = 1;
smiData.Solid(4).ID = 'Arm 02 v3*:*Default';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(5).mass = 0.033876630295594158;  % kg
smiData.Solid(5).CoM = [-0.49032191241283057 20.358074512903947 -1.2918440694522454e-06];  % mm
smiData.Solid(5).MoI = [5.8482510929244169 6.0274552077711148 9.5878080593298538];  % kg*mm^2
smiData.Solid(5).PoI = [6.4718830438005855e-07 1.5672389689865186e-07 0.27277211248685723];  % kg*mm^2
smiData.Solid(5).color = [0.2 0.2 0.2];
smiData.Solid(5).opacity = 1;
smiData.Solid(5).ID = 'Servo Motor MG996R*:*Default';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(6).mass = 0.067820595921070245;  % kg
smiData.Solid(6).CoM = [-6.4095291627549784 17.238890348287249 -1.3549795594461711];  % mm
smiData.Solid(6).MoI = [57.941485417266726 54.590163660091164 44.883528989834588];  % kg*mm^2
smiData.Solid(6).PoI = [6.1645507319202215 -0.63271222104507419 5.1507946253204988];  % kg*mm^2
smiData.Solid(6).color = [0.0 0.2 1.0];
smiData.Solid(6).opacity = 1;
smiData.Solid(6).ID = 'Waist*:*Default';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(7).mass = 0.095828460892655162;  % kg
smiData.Solid(7).CoM = [0.29387065312208671 22.277190102085417 -0.1540757612471039];  % mm
smiData.Solid(7).MoI = [127.26168940078622 207.82624245133653 138.17108572627612];  % kg*mm^2
smiData.Solid(7).PoI = [-0.12221029411324978 -0.0043876671422500239 4.0099041124211015];  % kg*mm^2
smiData.Solid(7).color = [0.2 0.2 0.2];
smiData.Solid(7).opacity = 1;
smiData.Solid(7).ID = 'Base*:*Default'; 

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(8).mass = 0.016436577268861788;  % kg
smiData.Solid(8).CoM = [0.046055291759741088 17.821347491415239 -5.2560867676757903];  % mm
smiData.Solid(8).MoI = [3.897860138821442 2.6117384631104175 5.0085962995801445];  % kg*mm^2
smiData.Solid(8).PoI = [0.5750619030919365 -0.0039761792758185926 0.011567867196576515];  % kg*mm^2
smiData.Solid(8).color = [0.0 0.2 1.0];
smiData.Solid(8).opacity = 1;
smiData.Solid(8).ID = 'Arm 03*:*Default';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(9).mass = 0.015666426054872281;  % kg
smiData.Solid(9).CoM = [15.938274089306509 -3.1873923114487219 -39.499572693325305];  % mm
smiData.Solid(9).MoI = [7.2355794590537803 8.0871020029619274 2.5430588628537434];  % kg*mm^2
smiData.Solid(9).PoI = [-1.3631869634418039 -1.419840550726726 -0.34963431449135224];  % kg*mm^2
smiData.Solid(9).color = [0.792156862745098 0.81960784313725488 0.93333333333333335];
smiData.Solid(9).opacity = 1;
smiData.Solid(9).ID = 'Gripper base*:*Default';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(10).mass = 0.0026416132846297426;  % kg
smiData.Solid(10).CoM = [-4.5275913705687669 2 -0.60196930727362119];  % mm
smiData.Solid(10).MoI = [0.090046111646969532 0.44723966749329935 0.3642378579386758];  % kg*mm^2
smiData.Solid(10).PoI = [0 -0.0092574084887821866 0];  % kg*mm^2
smiData.Solid(10).color = [0.2 0.2 0.2];
smiData.Solid(10).opacity = 1;
smiData.Solid(10).ID = 'gear1*:*Default';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(11).mass = 0.0025658596693438531;  % kg
smiData.Solid(11).CoM = [-15.802287599926878 2 0.13999953130627837];  % mm
smiData.Solid(11).MoI = [0.096871414429773928 0.44616255154541529 0.35613342956722499];  % kg*mm^2
smiData.Solid(11).PoI = [0 0.044672222615650257 0];  % kg*mm^2
smiData.Solid(11).color = [0.2 0.2 0.2];
smiData.Solid(11).opacity = 1;
smiData.Solid(11).ID = 'gear2*:*Default';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(12).mass = 0.0053974701398930717;  % kg
smiData.Solid(12).CoM = [4.2500225915138357 9.5131476517421962 -31.889976021591902];  % mm
smiData.Solid(12).MoI = [1.7139108532223841 1.5958276983903419 0.18307767030588401];  % kg*mm^2
smiData.Solid(12).PoI = [0.37341188434821881 6.5121955707659532e-06 1.2431071757242153e-05];  % kg*mm^2
smiData.Solid(12).color = [0.2 0.2 0.2];
smiData.Solid(12).opacity = 1;
smiData.Solid(12).ID = 'Gripper 1*:*Default';


%============= Joint =============%
%X Revolute Primitive (Rx) %Y Revolute Primitive (Ry) %Z Revolute Primitive (Rz)
%X Prismatic Primitive (Px) %Y Prismatic Primitive (Py) %Z Prismatic Primitive (Pz) %Spherical Primitive (S)
%Constant Velocity Primitive (CV) %Lead Screw Primitive (LS)
%Position Target (Pos)

%Initialize the CylindricalJoint structure array by filling in null values.
smiData.CylindricalJoint(6).Rz.Pos = 0.0;
smiData.CylindricalJoint(6).Pz.Pos = 0.0;
smiData.CylindricalJoint(6).ID = '';

%This joint has been chosen as a cut joint. Simscape Multibody treats cut joints as algebraic constraints to solve closed kinematic loops. The imported model does not use the state target data for this joint.
smiData.CylindricalJoint(1).Rz.Pos = -179.16770969229574;  % deg
smiData.CylindricalJoint(1).Pz.Pos = 0;  % mm
smiData.CylindricalJoint(1).ID = '[Gripper base-1:-:gear1-1]';

%This joint has been chosen as a cut joint. Simscape Multibody treats cut joints as algebraic constraints to solve closed kinematic loops. The imported model does not use the state target data for this joint.
smiData.CylindricalJoint(2).Rz.Pos = 179.56071134098977;  % deg
smiData.CylindricalJoint(2).Pz.Pos = 0;  % mm
smiData.CylindricalJoint(2).ID = '[Gripper base-1:-:grip link 1-3]';

smiData.CylindricalJoint(3).Rz.Pos = 179.73353269289962;  % deg
smiData.CylindricalJoint(3).Pz.Pos = 0;  % mm
smiData.CylindricalJoint(3).ID = '[Gripper base-1:-:grip link 1-4]';

%This joint has been chosen as a cut joint. Simscape Multibody treats cut joints as algebraic constraints to solve closed kinematic loops. The imported model does not use the state target data for this joint.
smiData.CylindricalJoint(4).Rz.Pos = 69.648173578118787;  % deg
smiData.CylindricalJoint(4).Pz.Pos = 0;  % mm
smiData.CylindricalJoint(4).ID = '[grip link 1-1:-:Gripper 1-2]';

%This joint has been chosen as a cut joint. Simscape Multibody treats cut joints as algebraic constraints to solve closed kinematic loops. The imported model does not use the state target data for this joint.
smiData.CylindricalJoint(5).Rz.Pos = 113.58445415636599;  % deg
smiData.CylindricalJoint(5).Pz.Pos = 0;  % mm
smiData.CylindricalJoint(5).ID = '[grip link 1-2:-:Gripper 1-1]';

%This joint has been chosen as a cut joint. Simscape Multibody treats cut joints as algebraic constraints to solve closed kinematic loops. The imported model does not use the state target data for this joint.
smiData.CylindricalJoint(6).Rz.Pos = -110.35182642188123;  % deg
smiData.CylindricalJoint(6).Pz.Pos = 0;  % mm
smiData.CylindricalJoint(6).ID = '[Gripper 1-2:-:grip link 1-4]';


%Initialize the PlanarJoint structure array by filling in null values.
smiData.PlanarJoint(1).Rz.Pos = 0.0;
smiData.PlanarJoint(1).Px.Pos = 0.0;
smiData.PlanarJoint(1).Py.Pos = 0.0;
smiData.PlanarJoint(1).ID = '';

%This joint has been chosen as a cut joint. Simscape Multibody treats cut joints as algebraic constraints to solve closed kinematic loops. The imported model does not use the state target data for this joint.
smiData.PlanarJoint(1).Rz.Pos = -0.17282135190985751;  % deg
smiData.PlanarJoint(1).Px.Pos = 0;  % mm
smiData.PlanarJoint(1).Py.Pos = 0;  % mm
smiData.PlanarJoint(1).ID = '[grip link 1-3:-:grip link 1-4]';


%Initialize the RevoluteJoint structure array by filling in null values.
smiData.RevoluteJoint(11).Rz.Pos = 0.0;
smiData.RevoluteJoint(11).ID = '';

smiData.RevoluteJoint(1).Rz.Pos = -12.217624006774042;  % deg
smiData.RevoluteJoint(1).ID = '[Base-1:-:Waist-1]';

smiData.RevoluteJoint(2).Rz.Pos = -2.1577592170911095;  % deg
smiData.RevoluteJoint(2).ID = '[Servo Motor Micro  9g-1:-:Arm 03-1]';

smiData.RevoluteJoint(3).Rz.Pos = -1.2873016895726055;  % deg
smiData.RevoluteJoint(3).ID = '[Servo Motor Micro  9g-2:-:Gripper base-1]';

smiData.RevoluteJoint(4).Rz.Pos = -173.37006907444405;  % deg
smiData.RevoluteJoint(4).ID = '[Gripper base-1:-:gear2-1]';

smiData.RevoluteJoint(5).Rz.Pos = -0.26646730710036515;  % deg
smiData.RevoluteJoint(5).ID = '[Gripper base-1:-:grip link 1-1]';

smiData.RevoluteJoint(6).Rz.Pos = 179.5607113409898;  % deg
smiData.RevoluteJoint(6).ID = '[Gripper base-1:-:grip link 1-2]';

smiData.RevoluteJoint(7).Rz.Pos = -113.58445415636601;  % deg
smiData.RevoluteJoint(7).ID = '[Gripper 1-1:-:grip link 1-3]';

smiData.RevoluteJoint(8).Rz.Pos = -106.51523457179982;  % deg
smiData.RevoluteJoint(8).ID = '[gear2-1:-:Gripper 1-1]';

smiData.RevoluteJoint(9).Rz.Pos = -111.45058403668587;  % deg
smiData.RevoluteJoint(9).ID = '[gear1-1:-:Gripper 1-2]';

smiData.RevoluteJoint(10).Rz.Pos = 135.41291211894989;  % deg
smiData.RevoluteJoint(10).ID = '[Servo Motor MG996R-2:-:Arm 01-1]';

smiData.RevoluteJoint(11).Rz.Pos = -89.443825333047556;  % deg
smiData.RevoluteJoint(11).ID = '[Arm 01-1:-:Servo Motor MG996R-7]';

